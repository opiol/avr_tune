/**
*******************************************************************************
* @file lcd_c.c
*
* @brief  LCD display library
* @author Peter Fleury, Tomas Fryza, Thomas Breining
* @date   Date/Time updated: Fri Nov  4 06:27:45 CET 2016
*
* The library defines constants and functions for the communication between
* HD44780 based LCD display and AVR microcontroller
*
* @par Target MCU: AVR, tested with ATmega16
* @par Compiler: avr-gcc, tested with avr-gcc (Fedora 4.5.0-2.el6) 4.5.0
*/

#include <avr/io.h>             // definition file for MCU ATmega16
#define F_CPU 16000000UL        // clock frequency for delay library
#include <util/delay.h>         // delay library
#include "lcd_h.h"              // LCD display library

unsigned char x, y;             // current cursor position


/**
************************************************************************
* @brief Performs initialization of LCD communication
*
* @return None
* @par Implementation notes:
*     - Within the function, port A (PA7:1) is configured as output
*     - Use initialization only once in the application
*/
void lcd_init(void)
{
    DDRA |= (1<<DDA7)|(1<<DDA6)|(1<<DDA5)|(1<<DDA4)|(1<<DDA3)|(1<<DDA2)|(1<<DDA1);
    _delay_ms(10);
    lcd_command(0b00100000);
    lcd_command(0b00101000);
    lcd_command(0b00000100);
    lcd_command(0b00001100);
    lcd_clrscr();
}

/**
************************************************************************
* @brief Clears LCD screen and moves cursor position to left upper corner
*
* @return None
* @par Implementation notes:
*     - Within the function, a short delay is called
*/
void lcd_clrscr(void)
{
    lcd_command(0b00000001);
    x = 0;
    y = 0;
    _delay_ms(10);
}

/**
************************************************************************
* @brief Sends 1-byte instruction to LCD display
*
* @param cmd    [IN] - LCD display instruction
* @return None
*/
void lcd_command(char cmd)
{
    lcd_write(cmd, 0);
}

/**
************************************************************************
* @brief Sends 1-byte data to LCD display
*
* @param data    [IN] - LCD display data
* @return None
*/
void lcd_data(char data)
{
    lcd_write(data, 1);
}

/**
************************************************************************
* @brief Sends 1-byte instruction (rs=0) or data (rs=1) to LCD display
*
* @param data    [IN] - LCD display instruction/data
* @param rs      [IN] - Instruction/data identifier
* @return None
* @par Implementation notes:
*     - Only 4-bit mode of LCD communication is supported
*     - Within the function, an enable pulse at E pin is generated
*/
void lcd_write(char data, char rs)
{
    char temp;

    if (rs)
        lcd_rs_high();          // data
    else
        lcd_rs_low();           // instruction
    temp = LCD_PORT & 0x0f;
    LCD_PORT = temp | (data&0xf0);
    lcd_e_toggle();
    LCD_PORT = temp | ((data&0x0f)<<4);
    lcd_e_toggle();
    LCD_PORT = temp | 0xf0;
    _delay_ms(2);
}

/**
************************************************************************
* @brief Generates enable pulse at pin E
* @return None
*/
void toggle_e(void)
{
    lcd_e_high();
    lcd_e_delay();
    lcd_e_low();
}

/**
************************************************************************
* @brief Moves LCD display cursor to new position
*
* @param x_new    [IN] - New horizontal position
* @param y_new    [IN] - New vertical position
* @return None
* @par Implementation notes:
*     - Coordinates (0,0) represent left upper corner of the LCD display
*/
void lcd_gotoxy(char x_new, char y_new)
{
    if (y_new==0) {
        lcd_command((1<<LCD_DDRAM)+LCD_START_LINE1+x_new);
        y = 0;
    }
    else if (y_new==1) {
        lcd_command((1<<LCD_DDRAM)+LCD_START_LINE2+x_new);
        y = 1;
    }
    else if (y_new==2) {
        lcd_command((1<<LCD_DDRAM)+LCD_START_LINE3+x_new);
        y = 2;
    }
    else if (y_new==3) {
        lcd_command((1<<LCD_DDRAM)+LCD_START_LINE4+x_new);
        y = 3;
    }
    x = x_new;
}

/**
************************************************************************
* @brief Puts single character to LCD display at current cursor position
*
* @param ch    [IN] - Character to be displayed
* @return None
* @par Implementation notes:
*     - Put character to apostrophes (e.g. lcd_putc('A')) or use the character's
*       ASCII code instead (e.g. lcd_putc(65))
*/
void lcd_putc(char ch)
{
    if (ch=='\n')
        lcd_newline();
    else {
        if (x==LCD_DISP_LENGTH && y==0)
            lcd_secondline();
        else if (x==LCD_DISP_LENGTH && y==1)
            lcd_thirdline();
        else if (x==LCD_DISP_LENGTH && y==2)
            lcd_fourthline();
        else if (x==LCD_DISP_LENGTH && y==3)
            lcd_firstline();
        lcd_data(ch);
        x++;
    }
}

/**
************************************************************************
* @brief Puts string to LCD display from current cursor position
*
* @param str    [IN] - String to be displayed
* @return None
* @par Implementation notes:
*     - Put string to quotation marks (e.g. lcd_puts("Hi there"))
*     - For a new line, use an "\n" identifier
*/
void lcd_put_string(const char *str)
{
    register char c;

    while (c=*str++)
        lcd_putc(c);
}

/**
************************************************************************
* @brief Moves cursor's position to the beginning of new line
*
* @return None
*/
void lcd_newline(void)
{
    if (y==3)
        lcd_firstline();
    else if (y==0)
        lcd_secondline();
    else if (y==1)
        lcd_thirdline();
    else if (y==2)
        lcd_fourthline();
}

/**
************************************************************************
* @brief Moves cursor's position to the beginning of the first line
*
* @return None
*/
void lcd_firstline(void)
{
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE1);
    x = 0;
    y = 0;
    lcd_put_string("                ");
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE1);
    x = 0;
    y = 0;
}

/**
************************************************************************
* @brief Moves cursor's position to the beginning of the second line
*
* @return None
*/
void lcd_secondline(void)
{
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE2);
    x = 0;
    y = 1;
    lcd_put_string("                ");
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE2);
    x = 0;
    y = 1;
}

/**
************************************************************************
* @brief Moves cursor's position to the beginning of the third line
*
* @return None
*/
void lcd_thirdline(void)
{
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE3);
    x = 0;
    y = 2;
    lcd_put_string("                ");
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE3);
    x = 0;
    y = 2;
}

/**
************************************************************************
* @brief Moves cursor's position to the beginning of the fourth line
*
* @return None
*/
void lcd_fourthline(void)
{
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE4);
    x = 0;
    y = 3;
    lcd_put_string("                ");
    lcd_command((1<<LCD_DDRAM)+LCD_START_LINE4);
    x = 0;
    y = 3;
}
