/**
*******************************************************************************
* @file lcd_h.h
*
* @brief  Header file for the LCD display library
* @author Peter Fleury, Tomas Fryza, Thomas Breining
* @date   Fri Nov  4 05:48:47 CET 2016
*/

#ifndef LCD_H_H_INCLUDED
#define LCD_H_H_INCLUDED

/**
*******************************************************************************
* @brief MCU port for communication with LCD display
*/
#define LCD_PORT PORTA

/**
*******************************************************************************
* @brief Position of RS (instruction/data) pin of LCD display
*/
#define LCD_RS_PIN 1

/**
*******************************************************************************
* @brief Position of R/W (read/write) pin of LCD display
*/
#define LCD_RW_PIN 2

/**
*******************************************************************************
* @brief Position of E (enable) pin of LCD display
*/
#define LCD_E_PIN 3

/**
*******************************************************************************
* @brief Length of one line on LCD display
*/
#define LCD_DISP_LENGTH 16

/**
*******************************************************************************
* @brief Instruction for DDRAM access
*/
#define LCD_DDRAM 7

/**
*******************************************************************************
* @brief Address of first line on LCD display
*/
#define LCD_START_LINE1 0x00

/**
*******************************************************************************
* @brief Address of second line on LCD display
*/
#define LCD_START_LINE2 0x40

/**
*******************************************************************************
* @brief Address of third line on LCD display
*/
#define LCD_START_LINE3 0x10

/**
*******************************************************************************
* @brief Address of fourth line on LCD display
*/
#define LCD_START_LINE4 0x50

/**
*******************************************************************************
* @brief Macro to set RS (instruction/data) pin high
*/
#define lcd_rs_high() LCD_PORT |= _BV(LCD_RS_PIN)

/**
*******************************************************************************
* @brief Macro to set RS (instruction/data) pin low
*/
#define lcd_rs_low() LCD_PORT &= ~_BV(LCD_RS_PIN)

/**
*******************************************************************************
* @brief Macro to set R/W (read/write) pin high
*/
#define lcd_rw_high() LCD_PORT |= _BV(LCD_RW_PIN)

/**
*******************************************************************************
* @brief Macro to set R/W (read/write) pin low
*/
#define lcd_rw_low() LCD_PORT &= ~_BV(LCD_RW_PIN)

/**
*******************************************************************************
* @brief Macro to short delay for three clock cycles
*/
#define lcd_e_delay() __asm__ __volatile__("nop\n nop\n nop")

/**
*******************************************************************************
* @brief Macro to set E (enable) pin high
*/
#define lcd_e_high() LCD_PORT |= _BV(LCD_E_PIN)

/**
*******************************************************************************
* @brief Macro to set E (enable) pin low
*/
#define lcd_e_low() LCD_PORT &= ~_BV(LCD_E_PIN)

/**
*******************************************************************************
* @brief Macro to generate one E (enable) pin pulse
*/
#define lcd_e_toggle() toggle_e()

void lcd_init(void);
void lcd_clrscr(void);
void lcd_command(char);
void lcd_data(char);
void lcd_write(char, char);
void toggle_e(void);
void lcd_gotoxy(char, char);
void lcd_putc(char);
void lcd_put_string(const char*);
void lcd_newline(void);
void lcd_firstline(void);
void lcd_secondline(void);
void lcd_thirdline(void);
void lcd_fourthline(void);

#endif // LCD_H_H_INCLUDED
