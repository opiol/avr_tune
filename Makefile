#** --------------------------------------------------------------------------*
TARGET = main
#** --------------------------------------------------------------------------*

FILES = lcd_c
OBJECT_FILES = $(TARGET).o $(FILES:%=%.o)
SRC_FILES = $(OBJECT_FILES:%.o=%.c)

CC = avr-gcc
CFLAGS = -mmcu=atmega16
#CFLAGS = -g -Wall -mcall-prologues -mmcu=$(DEVICE) -Os
AVRDUDE = avrdude
AVRDUDE_DEVICE = m16
#PROG = -c stk500
PROG = -c avrispmkII -P usb

all: $(TARGET).hex
%.obj: $(OBJECT_FILES)
	$(CC) $(CFLAGS) $(OBJECT_FILES) $(LDFLAGS) -o $@
%.hex: %.obj
	avr-objcopy -R .eeprom -O ihex $< $@
program: $(TARGET).hex
	$(AVRDUDE) -p $(AVRDUDE_DEVICE) $(PROG) -e -U flash:w:$(TARGET).hex
erase:
	$(AVRDUDE) -p $(AVRDUDE_DEVICE) $(PROG) -e
clean:
	rm -f *.o *.hex *.obj *~
