var menudata={children:[
{text:'Main Page',url:'index.html'},
{text:'Files',url:'files.html',children:[
{text:'File List',url:'files.html'},
{text:'Globals',url:'globals.html',children:[
{text:'All',url:'globals.html',children:[
{text:'b',url:'globals.html#index_b'},
{text:'d',url:'globals.html#index_d'},
{text:'e',url:'globals.html#index_e'},
{text:'g',url:'globals.html#index_g'},
{text:'l',url:'globals.html#index_l'},
{text:'p',url:'globals.html#index_p'},
{text:'t',url:'globals.html#index_t'}]},
{text:'Functions',url:'globals_func.html',children:[
{text:'e',url:'globals_func.html#index_e'},
{text:'g',url:'globals_func.html#index_g'},
{text:'l',url:'globals_func.html#index_l'},
{text:'p',url:'globals_func.html#index_p'},
{text:'t',url:'globals_func.html#index_t'}]},
{text:'Variables',url:'globals_vars.html'},
{text:'Macros',url:'globals_defs.html'}]}]}]}
