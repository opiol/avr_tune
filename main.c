/**
*******************************************************************************
* @file main.c
*
* @brief  avr buzzer music player
* @author Zbigniew Opiol, Jestribek Milan
*
*this program is waiting forinput from uart and plays it
*
* @par Target MCU: AVR, tested with ATmega16
*/

#define F_CPU 16000000UL
#define MAXBUF 768
//#define DEBUG 1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include <math.h>

#ifndef DEBUG
#include "lcd_h.h"
#endif

uint8_t buf[MAXBUF] = "50 500 4000 500 "; /**< buffer for incoming data */
uint8_t *ptr = buf;


#ifndef DEBUG
volatile uint8_t start = 0, play = 0;
#else
volatile uint8_t start = 1, play = 1;
#endif
volatile uint8_t data_locked = 0; /**< Buffer write protection */


/**
************************************************************************
* @brief manages error state
*
* @param errCode    [IN] - error code to manage
*/
void error(uint8_t errCode)
{
	UDR = errCode + '0';
}

ISR(TIMER1_COMPA_vect)
{
	PORTD = ~PORTD;
}

ISR(USART_RXC_vect)
{
	uint8_t data = UDR;
	
	switch(data)
	{
		case 'p':
		play = 1;
		break;
		case 's':
			start = 1;
			*ptr++ = 'x'; // hotfix when buffer end without newline
			*ptr = 0;
			ptr = buf; // return pointer to begining of buffer
		break;
		default:
		if (!data_locked)
		{	if(ptr < (buf + MAXBUF*sizeof(buf[0]) ) ) /* check buffer overflow /not on stack/  */
			{
				*ptr++ = data;
			}
			else
			{
			error(2); // buffer ovf error
			}
		}
		break;
	}
	
}


/**
************************************************************************
* @brief initializes peripherals
* @detail 9600 baud , 16 bit timer autoreload, div by 64
*/
void periphInit(void)
{
	DDRD = ( 1 << 7);
	TCCR1B =( 1 << CS11 ) | ( 1 << CS10 ) ; // div by 64
	TCCR1B |= ( 1 << WGM12 ); // ctc mode (OCR1A)
	TIMSK |= (1 << OCIE1A) ; // output compare reg

	/* uart 8N1 9600 baud */
	UBRRL = 103;
	UCSRC = ( 1 << 7 ) | ( 1 << UCSZ1 ) | ( 1 << UCSZ0 ) ;
	UCSRB = ( 1 << RXCIE) | ( 1 << RXEN ) |( 1 << TXEN ) ;
	sei();
}
/**
************************************************************************
* @brief parses one value from buffer 
*
* @param startString    [IN] - beginning of buffer
* @param stopString    [OUT] - new beginning of buffer / after parsed number
* @return parsed number
*/
int16_t getTone( uint8_t* startString, uint8_t** stopString)
{
	uint8_t len = 0;    /**< length of a number */
	uint8_t offset = 0; /**< start of a number from begining of a string*/
	int16_t value;
	
	if(startString[0] == 0)
	return -1; // end of string
	while( (startString[offset] < '0') || (startString[offset] > '9')) // not a number
	{
		offset++;
		if(startString[offset] == 0)
		return -1;
		
	}
	while( ((startString[offset+len]) >= '0') && (startString[offset+len] <= '9')) // is a number
	{
		len++;
		if(startString[offset] == 0)
		return -1;
	}
	startString[offset+len] = '\0';
	value = (uint16_t)atoi((char*)&startString[offset]);
	*stopString = &startString[offset+len+1]; // begining of next number
	return value;
	
}

char str[32];
int main(void)
{
	volatile int16_t* num = (int16_t *)buf;
	float tmpf;
	int i=0;
	uint16_t value = 0;
	uint8_t numbers = 0;

	periphInit();
	#ifndef DEBUG
	lcd_init();
	#endif

	start:
	TIMSK &= ~(1 << OCIE1A) ; // output compare reg

	#ifndef DEBUG
	lcd_clrscr();
	lcd_put_string("waiting");
	#endif

	
	while(!start); // wait for symbol
	data_locked = 1; // write protection from uart

	#ifndef DEBUG
	lcd_clrscr();
	#endif

	/* convert ascii numbers to uint16_t */
	while(-1 != ( num[numbers++] = getTone(ptr,&ptr)));

	/* check if numbers count is even */
	if ( numbers % 2)
	{
		error(0);
	}
	else
	{
		error(1);
		start = 0;
		goto start;
	}

	while(!play); // wait for symbol
	TIMSK |= (1 << OCIE1A) ;
	for( i = 0; i < numbers -1 ;i++)
	{
		tmpf = 125000;
		tmpf = tmpf / (float)num[i++];
		tmpf -= 1;
		value = (uint16_t)tmpf;
		
		/* check for pause: 0 Hz */
		if (value != 0)
		{
			TCNT1 = 0;
			OCR1AH =  (uint8_t)(value >> 8);
			OCR1AL =  (uint8_t)(value & 0xff);
			while(num[i]--)
			_delay_ms(1);
		}
		else
		{
			TIMSK &= ~(1 << OCIE1A) ;
			while(num[i]--)
			_delay_ms(1);
			TIMSK |= (1 << OCIE1A) ;
		}

	}

	TIMSK &= ~(1 << OCIE1A) ;
	error(0);
	play = 0;
	start = 0;
	data_locked = 0;
	goto start;
	
	
	while(1);
}
